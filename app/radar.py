from time import sleep

import math
import xlrd
from graphics import *

class grafico:
    def __init__(self, win, zoom):
        self.win = win
        self.zoom = zoom
        self.cont = 0
        win.setCoords(-400, -400, 400, 400)
        win.setBackground('black')

    def message(self, text):
        #Mensagem no canto inferior esquerdo da tela
        rect = Rectangle(Point(-20, -380), Point(-400, -400))
        rect.setFill('black')
        rect.draw(self.win)
        label = Text(Point(-(400 - (len(text) * 4)), -390), text)  # cada letra ocupa um espaço de 10 pixels
        label.setTextColor('white')
        label.setSize(12)
        label.draw(self.win)

    def label(self, dict, color):
        #Informações do avião
        text = f'{dict["nome"]}\n' \
            f'X = {dict["x"]}\n' \
            f'Y = {dict["y"]}'

        label = Text(Point(dict['x'], dict['y']+40), text)  # cada letra ocupa um espaço de 10 pixels
        label.setOutline(color)
        label.setSize(12)
        label.draw(self.win)

    def icone_aviao(self, x1, y1, x2, y2, x3, y3, voo):

        if voo['status'] == 'P':

            self.reta(x1, x2, y1, y2, 'red')

            self.reta(x1, x3, y1, y3, 'red')

            self.reta(x2, x3, y2, y3, 'red')

        if voo['status'] == 'D':
            self.reta(x1, x2, y1, y2, 'yellow')

            self.reta(x1, x3, y1, y3, 'yellow')

            self.reta(x2, x3, y2, y3, 'yellow')

    def aviao(self, voo):

        voo = self.projecao(voo)

        self.label(voo, 'white')

        x1 = 10

        y1 = 0

        x2 = -4

        y2 = 4

        x3 = -4

        y3 = -4

        # calcula a distancia do aviao ao centro
        d = math.sqrt(voo['x'] * voo['x'] + voo['y'] * voo['y'])

        if voo['x'] == 0 and voo['status'] == 'P':
            x1_rotacao = x1 * math.trunc(math.cos(math.radians(0)) - y1 * math.sin(math.radians(0)))

            y1_rotacao = y1 * math.trunc(math.cos(math.radians(0)) + x1 * math.sin(math.radians(0)))

            x2_rotacao = x2 * math.trunc(math.cos(math.radians(0)) - y2 * math.sin(math.radians(0)))

            y2_rotacao = y2 * math.trunc(math.cos(math.radians(0)) + x2 * math.sin(math.radians(0)))

            x3_rotacao = x3 * math.trunc(math.cos(math.radians(0)) - y3 * math.sin(math.radians(0)))

            y3_rotacao = y3 * math.trunc(math.cos(math.radians(0)) + x3 * math.sin(math.radians(0)))

            self.icone_aviao(x1_rotacao + voo['x'], y1_rotacao + voo['y'], x2_rotacao + voo['x'], y2_rotacao + voo['y'],
                             x3_rotacao + voo['x'], y3_rotacao + voo['y'], voo)

        if voo['x'] == 0 and voo['status'] == 'D':
            x1_rotacao = math.trunc(x1 * math.cos(math.radians(0) + math.pi) - y1 * math.sin(math.radians(0) + math.pi))

            y1_rotacao = math.trunc(y1 * math.cos(math.radians(0) + math.pi) + x1 * math.sin(math.radians(0) + math.pi))

            x2_rotacao = math.trunc(x2 * math.cos(math.radians(0) + math.pi) - y2 * math.sin(math.radians(0) + math.pi))

            y2_rotacao = math.trunc(y2 * math.cos(math.radians(0) + math.pi) + x2 * math.sin(math.radians(0) + math.pi))

            x3_rotacao = math.trunc(x3 * math.cos(math.radians(0) + math.pi) - y3 * math.sin(math.radians(0) + math.pi))

            y3_rotacao = math.trunc(y3 * math.cos(math.radians(0) + math.pi) + x3 * math.sin(math.radians(0) + math.pi))

            self.icone_aviao(x1_rotacao + voo['x'], y1_rotacao + voo['y'], x2_rotacao + voo['x'], y2_rotacao + voo['y'],
                             x3_rotacao + voo['x'], y3_rotacao + voo['y'], voo)

        if voo['x'] != 0:

            angulo = math.acos(voo['x'] / d)

            if voo['y'] < 0:
                angulo = angulo + math.pi

            if voo['x'] < 0 and voo['y'] < 0:
                angulo = angulo + math.pi + math.pi / 4

            if voo['x'] > 0 and voo['y'] < 0:
                angulo = angulo + (math.pi / 2)

            if voo['status'] == 'P':
                x1_rotacao = math.trunc(x1 * math.cos(angulo) - y1 * math.sin(angulo))

                y1_rotacao = math.trunc(y1 * math.cos(angulo) + x1 * math.sin(angulo))

                x2_rotacao = math.trunc(x2 * math.cos(angulo) - y2 * math.sin(angulo))

                y2_rotacao = math.trunc(y2 * math.cos(angulo) + x2 * math.sin(angulo))

                x3_rotacao = math.trunc(x3 * math.cos(angulo) - y3 * math.sin(angulo))

                y3_rotacao = math.trunc(y3 * math.cos(angulo) + x3 * math.sin(angulo))

                self.icone_aviao(x1_rotacao + voo['x'], y1_rotacao + voo['y'], x2_rotacao + voo['x'], y2_rotacao + voo['y'],
                                 x3_rotacao + voo['x'], y3_rotacao + voo['y'], voo)

            if voo['status'] == 'D':
                x1_rotacao = math.trunc(x1 * math.cos(angulo + math.pi) - y1 * math.sin(angulo + math.pi))

                y1_rotacao = math.trunc(y1 * math.cos(angulo + math.pi) + x1 * math.sin(angulo + math.pi))

                x2_rotacao = math.trunc(x2 * math.cos(angulo + math.pi) - y2 * math.sin(angulo + math.pi))

                y2_rotacao = math.trunc(y2 * math.cos(angulo + math.pi) + x2 * math.sin(angulo + math.pi))

                x3_rotacao = math.trunc(x3 * math.cos(angulo + math.pi) - y3 * math.sin(angulo + math.pi))

                y3_rotacao = math.trunc(y3 * math.cos(angulo + math.pi) + x3 * math.sin(angulo + math.pi))

                self.icone_aviao(x1_rotacao + voo['x'], y1_rotacao + voo['y'], x2_rotacao + voo['x'], y2_rotacao + voo['y'],
                                 x3_rotacao + voo['x'], y3_rotacao + voo['y'], voo)


    def projecao(self, dados):
            F = 800
            f = F * self.zoom

            x = dados['x']
            y = dados['y']
            z = dados['z']

            x_linha = (x * F) / (f - z)
            y_linha = (y * F) / (f - z)

            dados['x'] = int(x_linha)
            dados['y'] = int(y_linha)

            return dados

    def circle_create(self, x, y, color):
        self.ponto(x, y, color)
        self.ponto(y, x, color)
        self.ponto(y, -x, color)
        self.ponto(-x, y, color)
        self.ponto(-x, -y, color)
        self.ponto(-y, -x, color)
        self.ponto(-y, x, color)
        self.ponto(x, -y, color)

    def circulo(self, x, y, pc, color):
        self.circle_create(x, y, color)
        while x < y:
            x = x + 1
            if pc < 0:
                pc = pc + (2 * x) + 1
            else:
                y = y - 1
                pc = pc + (2 * x) + 1 - (2 * y)
            self.circle_create(x, y, color)

    def ponto(self, x, y, color, last=False):
        x = int(x)
        y = int(y)

        pt = Point(x, y)
        pt.setOutline(color)
        pt.draw(self.win)

    def reta(self, xi, xf, yi, yf, color=color_rgb(0, 255, 0), pintar=True, last=False):
        x = xi
        y = yi
        lista = []

        delta_x = xf - xi
        delta_y = yf - yi

        x_inc = 1
        y_inc = 1

        if delta_x < 0:
            x_inc = -1
            delta_x = -delta_x
        if delta_y < 0:
            y_inc = -1
            delta_y = -delta_y
        if delta_y <= delta_x:
            p = delta_x / 2
            while x != xf:
                if pintar:
                    self.ponto(x, y, color, last)
                else:
                    lista.append({'x': x, 'y': y})

                p = p - delta_y
                if p < 0:
                    y = y + y_inc
                    p = p + delta_x
                x = x + x_inc
        else:
            p = delta_y / 2
            while y != yf:
                if pintar:
                    self.ponto(x, y, color, last)
                else:
                    lista.append({'x': x, 'y': y})
                p = p - delta_x
                if p < 0:
                    x = x + x_inc
                    p = p + delta_y
                y = y + y_inc
        if pintar:
            self.ponto(x, y, color, last)
        else:
            lista.append({'x': x, 'y': y})
        return lista
