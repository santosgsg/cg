from time import sleep

import xlrd
from graphics import *

from app.radar import grafico

circ = []
reta = []
v = 400
c = v - 100
cor = color_rgb(0, 255, 0)

book = xlrd.open_workbook(f"C:\\Users\\{os.getlogin()}\\Downloads\\plnilha de radar.xlsx")
sh = book.sheet_by_index(0)
row = 0
voo = {}
last = {}
direction = {}
red = color_rgb(255, 0, 0)
black = color_rgb(0, 0, 0)
inicio = True
zoom = 50

for r in sh.get_rows():
    nome = sh.cell_value(rowx=row, colx=2)
    if nome == 'Voo':
        row += 1
        continue
    elif nome == '':
        inicio = True
        sleep(2)
        row += 1
        win.close()
        continue

    if inicio:
        win = GraphWin('', 800, 800)
        win.master.geometry('%dx%d+%d+%d' % (800, 800, 200, -50))
        g = grafico(win=win, zoom=zoom)

        g.message('Renderizando mapa [#-------]')
        g.grph = g.circulo(0, c / 4, c / 4 - 1, cor)
        g.message('Renderizando mapa [##------]')
        g.grph = g.circulo(0, c / 2, c / 2 - 1, cor)
        g.message('Renderizando mapa [###-----]')
        g.grph = g.circulo(0, c, c - 1, cor)
        g.message('Renderizando mapa [####----]')

        g.grph = g.reta(-v, v, v, -v)
        g.message('Renderizando mapa [#####---]')
        g.grph = g.reta(v, -v, v, -v)
        g.message('Renderizando mapa [######--]')
        g.grph = g.reta(v, -v, 0, 0)
        g.message('Renderizando mapa [#######-]')
        g.grph = g.reta(0, 0, v, -v)
        g.message('Renderizando mapa [########]')

        g.message('Renderização concluída!')
        g.message(f'Zoom: {zoom}')
        inicio = False

        while True:
            k = win.checkKey()
            if k == 'Up':
                print(zoom)
                zoom += 40
                break
            elif k == 'Down':
                print(zoom)
                zoom -= 40
                if zoom < 0:
                    g.message('Os dados serão espelhados, deseja continuar?')
                    continue
                break
            elif k == 'Left':
                break
            elif k == 'Right':
                break

    voo[nome] = {
        'Velocidade': sh.cell_value(rowx=row, colx=4),
        'x': int(sh.cell_value(rowx=row, colx=5)),
        'y': int(sh.cell_value(rowx=row, colx=6)),
        'z': int(sh.cell_value(rowx=row, colx=7)),
        'nome': nome,
        'status': sh.cell_value(rowx=row, colx=1)
    }

    g.aviao(voo[nome])

    row += 1

g.message('Finalizado')

sleep(10)
